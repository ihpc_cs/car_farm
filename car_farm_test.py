"""
Usage example using trained model to predict accuracy for given dataset  -- xulei 22/4/2016
"""

from __future__ import print_function
import sys
import os
import cv2
import glob
import math
import time
import random
import numpy as np
import pandas as pd

from sklearn.utils import shuffle
from sklearn.preprocessing import LabelEncoder
from sklearn.cross_validation import train_test_split

import lasagne
from lasagne.layers import helper
from lasagne.updates import adam
from lasagne.nonlinearities import rectify, softmax
from lasagne.layers import InputLayer, MaxPool2DLayer, DenseLayer, DropoutLayer, helper
from lasagne.layers import Conv2DLayer as ConvLayer

import theano
from theano import tensor as T

import cPickle


use_cache = 1
# color type: 1 - grey, 3 - rgb
color_type_global = 1

#image size = 480 x 640
imgH = 48
imgW = 64
imgC = color_type_global 
imageSize = imgC * imgH * imgW
num_features = imageSize 

def cache_data(data, path):
    if os.path.isdir(os.path.dirname(path)):
        file = open(path, 'wb')
        pickle.dump(data, file)
        file.close()
    else:
        print('Directory doesnt exists')


def restore_data(path):
    data = dict()
    if os.path.isfile(path):
        file = open(path, 'rb')
        data = pickle.load(file)
    return data

def get_im_cv2(path, img_rows, img_cols, color_type=1):
    # Load as grayscale
    if color_type == 1:
        img = cv2.imread(path, 0)
    elif color_type == 3:
        img = cv2.imread(path)
    # Reduce size
    resized = cv2.resize(img, (img_cols, img_rows))
    return resized

def load_test(img_rows, img_cols, color_type=1):
    print('Read test images')
    path = os.path.join('.', 'input', 'test', '*.jpg')
    files = glob.glob(path)
    #files = files1[:1000]
    X_test = []
    X_test_id = []
    total = 0
    thr = math.floor(len(files)/10)
    for fl in files:
        flbase = os.path.basename(fl)
        img = get_im_cv2(fl, img_rows, img_cols, color_type)
        X_test.append(img)
        X_test_id.append(flbase)
        total += 1
        if total%thr == 0:
            print('Read {} images from {}'.format(total, len(files)))

    return X_test, X_test_id


def read_and_normalize_test_data(img_rows, img_cols, color_type=1):
    cache_path = os.path.join('cache', 'test_r_' + str(img_rows) + '_c_' + str(img_cols) + '_t_' + str(color_type) + '.dat')
    if not os.path.isfile(cache_path) or use_cache == 0:
        test_data, test_id = load_test(img_rows, img_cols, color_type)
        cache_data((test_data, test_id), cache_path)
    else:
        print('Restore test from cache!')
        (test_data, test_id) = restore_data(cache_path)

    test_data = np.array(test_data, dtype=np.uint8)
    test_data = test_data.reshape(test_data.shape[0], color_type, img_rows, img_cols)
    test_data = test_data.astype('float32')
    test_data /= 255
    print('Test shape:', test_data.shape)
    print(test_data.shape[0], 'test samples')
    return test_data, test_id


def DCNN7(input_var=None):

    network = lasagne.layers.InputLayer(shape=(None, imgC, imgH, imgW),
                                        input_var=input_var)

    # Convolutional layer with 32 kernels of size 3x3. Strided and padded
    # convolutions are supported as well; see the docstring.

    network = lasagne.layers.Conv2DLayer(
            network, num_filters=16, filter_size=(3, 3),pad='same',
            nonlinearity=lasagne.nonlinearities.rectify,
            W=lasagne.init.GlorotUniform())

    # Max-pooling layer of factor 2 in both dimensions:
    network = lasagne.layers.MaxPool2DLayer(network, pool_size=(2, 2))


    network = lasagne.layers.Conv2DLayer(
            network, num_filters=32, filter_size=(3, 3),pad='same',
            nonlinearity=lasagne.nonlinearities.rectify,
            W=lasagne.init.GlorotUniform())

    # Max-pooling layer of factor 2 in both dimensions:
    network = lasagne.layers.MaxPool2DLayer(network, pool_size=(2, 2))

    # Another convolution with 32 3x3 kernels, and another 2x2 pooling:
    network = lasagne.layers.Conv2DLayer(
            network, num_filters=64, filter_size=(3, 3),pad='same',
            nonlinearity=lasagne.nonlinearities.rectify)
    network = lasagne.layers.MaxPool2DLayer(network, pool_size=(2, 2))

    # Another convolution with 64 3x3 kernels, and another 2x2 pooling:
    #network = lasagne.layers.Conv2DLayer(
    #        network, num_filters=128, filter_size=(3, 3),pad='same',
    #        nonlinearity=lasagne.nonlinearities.rectify)
    #network = lasagne.layers.MaxPool2DLayer(network, pool_size=(2, 2))

    # A fully-connected layer of 256 units with 50% dropout on its inputs:
    network = lasagne.layers.DenseLayer(
            lasagne.layers.dropout(network, p=.5),
            num_units=256,
            nonlinearity=lasagne.nonlinearities.rectify)

    # And, finally, the 2-unit output layer with 50% dropout on its inputs:
    network = lasagne.layers.DenseLayer(
            lasagne.layers.dropout(network, p=.5),
            num_units=10,
            nonlinearity=lasagne.nonlinearities.softmax)

    return network

# ############################# Batch iterator ###############################
# This is just a simple helper function iterating over training data in
# mini-batches of a particular size, optionally in random order. It assumes
# data is available as numpy arrays. For big datasets, you could load numpy
# arrays as memory-mapped files (np.load(..., mmap_mode='r')), or write your
# own custom data iteration function. For small datasets, you can also copy
# them to GPU at once for slightly improved performance. This would involve
# several changes in the main program, though, and is not demonstrated here.

def iterate_minibatches(inputs, targets, batchsize, shuffle=False):
    assert len(inputs) == len(targets)
    if shuffle:
        indices = np.arange(len(inputs))
        np.random.shuffle(indices)
    for start_idx in range(0, len(inputs) - batchsize + 1, batchsize):
        if shuffle:
            excerpt = indices[start_idx:start_idx + batchsize]
        else:
            excerpt = slice(start_idx, start_idx + batchsize)
        yield inputs[excerpt], targets[excerpt]


# ############################## Main program ################################
# Everything else will be handled in our main program now. We could pull out
# more functions to better separate the code, but it wouldn't make it any
# easier to read.

def main(fmodel='./mod_log/model_12.npz',fname='/home/xulei/Mozi/data/bti5418/12875/test_batch'):
    

    img_rows, img_cols = imgH, imgW

    # Load the dataset
    print("Loading data:" + fname)
    #X_test, y_test = load_dataset(fname)
    X_test, y_test= read_and_normalize_test_data(img_rows, img_cols, color_type_global)

    #import pdb; pdb.set_trace()
    print ("data length:%d", len(y_test)) 
    #Prepare Theano variables for inputs and targets

    input_var = T.tensor4('inputs')
    target_var = T.ivector('targets')
    
    network = DCNN7(input_var)
    # Create neural network model (depending on first command line parameter)
  

    # Create a loss expression for validation/testing. The crucial difference
    # here is that we do a deterministic forward pass through the network,
    # disabling dropout layers.
    test_prediction = lasagne.layers.get_output(network, deterministic=True)
    test_loss = lasagne.objectives.categorical_crossentropy(test_prediction,
                                                            target_var)
    test_loss = test_loss.mean()
    # As a bonus, also create an expression for the classification accuracy:
    test_acc = T.mean(T.eq(T.argmax(test_prediction, axis=1), target_var),
                      dtype=theano.config.floatX)

   # Compile a second function computing the validation loss and accuracy:
    val_fn = theano.function([input_var, target_var], [test_loss, test_acc])

    
    # set up prediction function
    predict_proba = theano.function(inputs=[input_var], outputs=test_prediction)


    print("Loading model:" + fmodel)
    with np.load(fmodel) as f:
        param_values = [f['arr_%d' % i] for i in range(len(f.files))]
    lasagne.layers.set_all_param_values(network, param_values)

    '''
    # After training, we compute and print the test error:
    test_err = 0
    test_acc = 0
    test_batches = 0
    for batch in iterate_minibatches(X_test, y_test, 1, shuffle=False):
        inputs, targets = batch
        #import pdb; pdb.set_trace()
        err, acc = val_fn(inputs, targets)
        test_err += err
        test_acc += acc
      	test_batches += 1
    print("Final results:")
    print("  test loss:\t\t\t{:.6f}".format(test_err / test_batches))
    print("  test accuracy:\t\t{:.2f} %".format(
        test_acc / test_batches * 100))
    '''

    '''
    Make Submission
    '''

    #make predictions
    print('Making predictions')
    PRED_BATCH = 2
    def iterate_pred_minibatches(inputs, batchsize):
        for start_idx in range(0, len(inputs) - batchsize + 1, batchsize):
            excerpt = slice(start_idx, start_idx + batchsize)
            yield inputs[excerpt]

    predictions = []
    for pred_batch in iterate_pred_minibatches(X_test, PRED_BATCH):
        predictions.extend(predict_proba(pred_batch))

    predictions = np.array(predictions)

    print('pred shape')
    print(predictions.shape)

    print('Creating Submission')
    def create_submission(predictions, test_id):
        result1 = pd.DataFrame(predictions, columns=['c0', 'c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9'])
        result1.loc[:, 'img'] = pd.Series(test_id, index=result1.index)
        result1.to_csv('submission1.csv', index=False)

    create_submission(predictions, y_test)




if __name__ == '__main__':
    if ('--help' in sys.argv) or ('-h' in sys.argv):
        print("Trains a neural network on MNIST using Lasagne.")
        print("Usage: %s [MODEL [EPOCHS]]" % sys.argv[0])
        print()
        print("fmodel: the name of the previously trained model")
        print("fname: the name of the testing dataset")
    else:
        kwargs = {}
        if len(sys.argv) > 1:
            kwargs['fmodel'] = sys.argv[1]
        if len(sys.argv) > 2:
            kwargs['fname'] = sys.argv[2]
        main(**kwargs)
